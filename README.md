# fdio-server

This provides a containerized version of FdIOServer: https://git.ligo.org/virgo/virgoapp/Fd

## Usage

```
docker run -it containers.ligo.org/patrick.godwin/fdio-server:main <Config<CmName>[.cfg]> ... <CmName>
```
